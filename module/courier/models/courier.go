package models

type Courier struct {
	Score    int   `json:"score"`
	Location Point `json:"location"`
}

type Point struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

func (m *Courier) TableName() string {
	return "courier"
}

func (m *Courier) OnCreate() []string {
	return []string{}
}

func (m *Point) TableName() string {
	return "point"
}

func (m *Point) OnCreate() []string {
	return []string{}
}
