package models

import "gitlab.com/GoGerman/go-geo/db/types"

type CourierAdapter struct {
	Score int               `json:"score" db:"score" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	Lat   types.NullFloat64 `json:"lat" db:"lat" db_type:"numeric" db_default:"default 0" db_ops:"create,update"`
	Lng   types.NullFloat64 `json:"lng" db:"lng" db_type:"numeric" db_default:"default 0" db_ops:"create,update"`
}

func (m *CourierAdapter) TableName() string {
	return "courier"
}

func (m *CourierAdapter) OnCreate() []string {
	return []string{}
}
