package service

import (
	"context"
	"gitlab.com/GoGerman/go-geo/geo"
	"gitlab.com/GoGerman/go-geo/module/courier/models"
	"gitlab.com/GoGerman/go-geo/module/courier/storage"
	"math"
)

// Направления движения курьера
const (
	DirectionUp    = 0
	DirectionDown  = 1
	DirectionLeft  = 2
	DirectionRight = 3
)

const (
	DefaultCourierLat = 59.9311
	DefaultCourierLng = 30.3609
)

//go:generate mockery --name Courierer

type Courierer interface {
	GetCourier(ctx context.Context) (*models.Courier, error)
	MoveCourier(courier models.Courier, direction, zoom int) error
}

type CourierService struct {
	courierStorage storage.CourierStorager
	allowedZone    geo.PolygonChecker
	disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disabledZones []geo.PolygonChecker) Courierer {
	return &CourierService{courierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disabledZones}
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {
	// получаем курьера из хранилища используя метод GetOne из storage/courier.go
	var courier models.Courier
	cr, err := c.courierStorage.GetOne(ctx)
	if err != nil {
		return nil, err
	}

	if cr == nil {
		courier.Location.Lng = DefaultCourierLng
		courier.Location.Lat = DefaultCourierLat
		courier.Score = 0
	} else {
		courier = *cr
	}

	// проверяем, что курьер находится в разрешенной зоне
	// если нет, то перемещаем его в случайную точку в разрешенной зоне
	if !geo.CheckPointIsAllowed(geo.Point(courier.Location), c.allowedZone, c.disabledZones) {
		courier.Location = models.Point(geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones))
	}
	// сохраняем новые координаты курьера

	err = c.courierStorage.Save(ctx, courier)
	if err != nil {
		return nil, err
	}
	return &courier, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(courier models.Courier, direction, zoom int) error {
	// точность перемещения зависит от зума карты использовать формулу 0.001 / 2^(zoom - 14)
	// 14 - это максимальный зум карты
	accuracy := 0.0001 / math.Pow(2, float64(zoom-17))

	switch direction {
	case DirectionUp:
		courier.Location.Lat += accuracy
	case DirectionDown:
		courier.Location.Lat -= accuracy
	case DirectionLeft:
		courier.Location.Lng -= accuracy
	case DirectionRight:
		courier.Location.Lng += accuracy
	}

	// далее нужно проверить, что курьер не вышел за границы зоны
	// если вышел, то нужно переместить его в случайную точку внутри зоны
	if !geo.CheckPointIsAllowed(geo.Point(courier.Location), c.allowedZone, c.disabledZones) {
		point := geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		courier.Location = models.Point(point)
	}
	// далее сохранить изменения в хранилище
	err := c.courierStorage.Save(context.Background(), courier)
	if err != nil {
		return err
	}
	return nil
}
