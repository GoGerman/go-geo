package storage

import (
	"context"
	"gitlab.com/GoGerman/go-geo/db/adapter"
	"gitlab.com/GoGerman/go-geo/db/types"
	"gitlab.com/GoGerman/go-geo/module/courier/models"
)

//go:generate mockery --name CourierStorager

type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	sqlAdapter *adapter.SQLAdapter
}

func NewCourierStorage(sqlAdapter *adapter.SQLAdapter) CourierStorager {
	return &CourierStorage{sqlAdapter: sqlAdapter}
}

func (c *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	adapt := models.CourierAdapter{
		Lat:   types.NewNullFloat64(courier.Location.Lat),
		Lng:   types.NewNullFloat64(courier.Location.Lng),
		Score: courier.Score,
	}
	err := c.sqlAdapter.Create(ctx, &adapt)
	if err != nil {
		return err
	}
	return nil
}

func (c *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	var list []models.CourierAdapter
	var adapt models.CourierAdapter
	err := c.sqlAdapter.List(ctx, &list, adapt.TableName(), adapter.Condition{})
	if err != nil {
		return nil, nil
	}
	if len(list) == 0 {
		return &models.Courier{}, nil
	} else {
		return &models.Courier{
			Location: models.Point{
				Lng: list[len(list)-1].Lng.Float64,
				Lat: list[len(list)-1].Lat.Float64,
			},
			Score: list[len(list)-1].Score,
		}, nil
	}
}
