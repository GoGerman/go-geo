package models

import (
	cm "gitlab.com/GoGerman/go-geo/module/courier/models"
	om "gitlab.com/GoGerman/go-geo/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}

func (m *CourierStatus) TableName() string {
	return "courierStatus"
}

func (m *CourierStatus) OnCreate() []string {
	return []string{}
}
