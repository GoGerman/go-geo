package controller

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/GoGerman/go-geo/metrics"
	"gitlab.com/GoGerman/go-geo/module/courierfacade/service"
	"log"
	"net/http"
	"time"
)

type CourierController struct {
	courierService service.CourierFacer
	metrics        *metrics.Metrics
}

func NewCourierController(courierService service.CourierFacer, metrics *metrics.Metrics) *CourierController {
	return &CourierController{courierService: courierService, metrics: metrics}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд
	time.Sleep(50 * time.Millisecond)

	// увеличить количество запросов
	c.metrics.RequestCount.Inc()
	// посчитать время запроса
	start := time.Now()
	defer func() {
		c.metrics.OrderRequestExecutionTime.Observe(time.Since(start).Seconds())
	}()

	// получить статус курьера из сервиса courierService используя метод GetStatus
	// отправить статус курьера в ответ
	status := c.courierService.GetStatus(ctx)

	// установить метрику количества заказов возле курьера
	c.metrics.OrdersNearCourier.Set(float64(len(status.Orders)))

	// Отправить статус курьера в ответ
	ctx.JSON(http.StatusOK, status)
}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	// увеличить количество запросов
	c.metrics.RequestCount.Inc()
	// посчитать время запроса
	start := time.Now()
	defer func() {
		c.metrics.CourierRequestExecutionTime.Observe(time.Since(start).Seconds())
	}()

	var cm CourierMove
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	err := json.Unmarshal((m.Data).([]byte), &cm)
	if err != nil {
		log.Println(err)
		return
	}
	// вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
}
