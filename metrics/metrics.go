package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type Metrics struct {
	RequestCount                prometheus.Counter
	OrdersNearCourier           prometheus.Gauge
	OrderRequestExecutionTime   prometheus.Histogram
	CourierRequestExecutionTime prometheus.Histogram
}

func NewMetrics() *Metrics {
	requestCount := promauto.NewCounter(
		prometheus.CounterOpts{
			Namespace: "courier",
			Name:      "http_request_count",
			Help:      "Total number of HTTP requests.",
		})

	ordersNearCourier := promauto.NewGauge(
		prometheus.GaugeOpts{
			Namespace: "courier",
			Name:      "orders_near_courier",
			Help:      "Total number of HTTP requests.",
		})

	orderRequestExecutionTime := promauto.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: "courier",
			Name:      "http_request_execution_time_order",
			Help:      "Time taken to complete the order request",
		})

	courierRequestExecutionTime := promauto.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: "courier",
			Name:      "http_request_execution_time_courier",
			Help:      "Time taken to complete the courier request",
		})

	prometheus.MustRegister(requestCount, orderRequestExecutionTime, ordersNearCourier, orderRequestExecutionTime)
	return &Metrics{
		RequestCount:                requestCount,
		OrdersNearCourier:           ordersNearCourier,
		OrderRequestExecutionTime:   orderRequestExecutionTime,
		CourierRequestExecutionTime: courierRequestExecutionTime,
	}
}
