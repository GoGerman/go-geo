package run

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/GoGerman/go-geo/cache"
	"gitlab.com/GoGerman/go-geo/config"
	"gitlab.com/GoGerman/go-geo/db"
	"gitlab.com/GoGerman/go-geo/db/migrate"
	"gitlab.com/GoGerman/go-geo/db/scanner"
	"gitlab.com/GoGerman/go-geo/geo"
	graf "gitlab.com/GoGerman/go-geo/metrics"
	c "gitlab.com/GoGerman/go-geo/module/courier/models"
	cservice "gitlab.com/GoGerman/go-geo/module/courier/service"
	cstorage "gitlab.com/GoGerman/go-geo/module/courier/storage"
	"gitlab.com/GoGerman/go-geo/module/courierfacade/controller"
	cfservice "gitlab.com/GoGerman/go-geo/module/courierfacade/service"
	oservice "gitlab.com/GoGerman/go-geo/module/order/service"
	ostorage "gitlab.com/GoGerman/go-geo/module/order/storage"
	"gitlab.com/GoGerman/go-geo/router"
	"gitlab.com/GoGerman/go-geo/server"
	"gitlab.com/GoGerman/go-geo/workers/order"
	"go.uber.org/zap"
	"log"
	"net/http"
	"os"
)

type App struct {
	conf config.AppConf
}

func NewApp(conf config.AppConf) *App {
	return &App{
		conf: conf,
	}
}

func (a *App) Run() error {
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&c.CourierAdapter{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner)
	if err != nil {
		log.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		log.Fatal("migrator err", zap.Error(err))
	}

	// инициализация клиента redis
	rclient := cache.NewRedisClient(
		os.Getenv("CACHE_ADDRESS"),
		os.Getenv("CACHE_PORT"),
		os.Getenv("CACHE_PASSWORD"))
	// проверка доступности redis
	_, err = rclient.Ping(context.Background()).Result()
	if err != nil {
		log.Println(1)
		return err
	}

	rclient.FlushAll(context.Background())

	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone()
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{
		geo.NewDisAllowedZone1(),
		geo.NewDisAllowedZone2(),
	}
	log.Println(2)
	// инициализация хранилища заказов
	orderStorage := ostorage.NewOrderStorage(rclient)
	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация хранилища курьеров
	courierStorage := cstorage.NewCourierStorage(sqlAdapter)

	// инициализация сервиса курьеров
	courierService := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierService, orderService)

	metrics := graf.NewMetrics()
	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade, metrics)

	// инициализация роутера
	routes := router.NewRouter(courierController)

	//gin.SetMode(gin.ReleaseMode)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	routes.Prometheus(mainRoute)
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
