package db

import (
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/GoGerman/go-geo/config"
	"gitlab.com/GoGerman/go-geo/db/adapter"
	"gitlab.com/GoGerman/go-geo/db/scanner"
	"go.uber.org/zap"
	"log"
	"time"
)

func NewSqlDB(dbConf config.DB, scanner scanner.Scanner) (*sqlx.DB, *adapter.SQLAdapter, error) {
	var dsn string
	var err error
	var dbRaw *sql.DB

	dsn = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", dbConf.Host, dbConf.Port, dbConf.User, dbConf.Password, dbConf.Name)

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	timeoutExceeded := time.After(time.Second * time.Duration(dbConf.Timeout))

	for {
		select {
		case <-timeoutExceeded:
			return nil, nil, fmt.Errorf("db connection failed after %d timeout %s", dbConf.Timeout, err)
		case <-ticker.C:
			dbRaw, err = sql.Open(dbConf.Driver, dsn)
			if err != nil {
				return nil, nil, err
			}
			err = dbRaw.Ping()
			if err == nil {
				db := sqlx.NewDb(dbRaw, dbConf.Driver)
				db.SetMaxOpenConns(50)
				db.SetMaxIdleConns(50)
				sqlAdapter := adapter.NewSqlAdapter(db, dbConf, scanner)
				return db, sqlAdapter, nil
			}
			log.Println("failed to connect to the database", zap.String("dsn", dsn), zap.Error(err))
		}
	}
}
