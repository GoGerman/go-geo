package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"gitlab.com/GoGerman/go-geo/config"
	"gitlab.com/GoGerman/go-geo/run"
	"log"
	"os"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Println(fmt.Sprintf("error: %s", err))
		os.Exit(1)
	}

	conf := config.NewAppConf()
	conf.Init()

	// инициализация приложения
	app := run.NewApp(conf)
	if err = app.Run(); err != nil {
		log.Println(fmt.Sprintf("error: %s", err))
		os.Exit(2)
	}
}
